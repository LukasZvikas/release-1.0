import React from 'react';
import PropTypes from 'prop-types';
import '../components/videoPlayer/videoPlayer.css';


const StarIcon = ({ color }) => (
  <svg
    x="0px"
    y="0px"
    height="20px"
    width="20px"
    viewBox="0 0 53.867 53.867"
    fill={color || 'yellow'}
  >
    <polygon
      points="26.934,1.318 35.256,18.182 53.867,20.887 40.4,34.013 43.579,52.549 26.934,43.798
    10.288,52.549 13.467,34.013 0,20.887 18.611,18.182 "
    />

  </svg>

);

StarIcon.propTypes = {
  color: PropTypes.string,
};

StarIcon.defaultProps = {
  color: '#fff',
};
export default StarIcon;
