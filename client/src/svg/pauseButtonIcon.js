import React from 'react';
import PropTypes from 'prop-types';
import '../components/videoPlayer/videoPlayer.css';


const PauseButtonIcon = ({ color }) => (
  <svg
    x="0px"
    y="0px"
    width="20px"
    height="20px"
    style={{ fill: color }}
    viewBox="0 0 357 357"
  >
    <path d="M25.5,357h102V0h-102V357z M229.5,0v357h102V0H229.5z" />
  </svg>

);

PauseButtonIcon.propTypes = {
  color: PropTypes.string,
};

PauseButtonIcon.defaultProps = {
  color: '#fff',
};
export default PauseButtonIcon;
