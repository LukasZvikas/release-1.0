import React from 'react';
import PropTypes from 'prop-types';
import './videoButton.css';

const VideoButton = ({ action, children }) => (
  <div
    onClick={() => action()}
    onKeyDown={event => event.keyCode === 32 && action()
    }
    className="video__button d-flex align-items-center"
    role="button"
    tabIndex="0"
  >
    {children}
  </div>
);

VideoButton.propTypes = {
  action: PropTypes.func.isRequired,
  children: PropTypes.element.isRequired,
};

export default VideoButton;
