import React, { Component, createRef } from 'react';
import './videoPlayer.css';
import VideoButton from './videoButton';
import ProgressBar from './progressBar';
import PlayButtonIcon from '../../svg/playButtonIcon';
import PauseButtonIcon from '../../svg/pauseButtonIcon';
import ReplayButtonIcon from '../../svg/replayButtonIcon';

class VideoPlayer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      playing: false, duration: 0, currentTime: 0, currentProgress: 0,
    };
    this.videoElement = createRef();
    this.videoURL = 'https://playsightproductionusea2.gcdn.co/files/Analytics/19-04-13/EDITED_6ac1fff4ad8d418e8bb50ceeca6d803e_Court2___13-04-2019___12-48-27___14-36-55__001__2L_1L_128.235.227.27__70d6c0bf.mp4';
  }

  updateProgress= () => {
    const { videoElement: { current: { currentTime, duration } } } = this;
    // Calculate current video progress
    const currentProgress = (100 / duration) * currentTime;
    this.setState({ duration, currentTime, currentProgress });
  };

  onProgressChange = (timeVal) => {
    const { duration } = this.state;
    // Calculate new current video progress from specific time-frame in the video
    const time = (duration * timeVal) / 100;
    this.videoElement.current.currentTime = time;
    window.history.pushState(null, '', `video?time_frame=${time}`);
    this.setState({ currentProgress: timeVal, currentTime: time });
  }

  updateAnnotations = (time) => {
    const { selectedFrames } = this.state;
    this.setState({ selectedFrames: [...selectedFrames, time] });
  }

  onVideoLoad = (duration) => {
    this.setState({ duration });
  }

  togglePlayingState = () => {
    const { playing } = this.state;
    const { videoElement } = this;
    if (playing) {
      videoElement.current.pause();
      this.setState({ playing: false });
    } else {
      videoElement.current.play();
      this.setState({ playing: true });
    }
  };

  restartVideo = () => {
    const { videoElement } = this;
    videoElement.current.currentTime = 0;
    videoElement.current.play();
    this.setState({ playing: true });
  }

  renderPlayingButton = () => {
    const { playing, currentTime, duration } = this.state;

    if (currentTime && currentTime === duration) {
      return <VideoButton action={this.restartVideo}><ReplayButtonIcon /></VideoButton>;
    }
    return (
      <VideoButton action={this.togglePlayingState}>
        {playing ? <PauseButtonIcon /> : <PlayButtonIcon />}
      </VideoButton>
    );
  }

  render() {
    const { currentProgress } = this.state;

    return (
      <div className="d-flex justify-content-center align-items-center flex-column mt-10">
        <div className="video d-flex">
          <video
            onLoadedMetadata={e => this.onVideoLoad(e.target.duration)}
            onTimeUpdate={() => { this.updateProgress(); }}
            className="video__player"
            ref={this.videoElement}
            src={this.videoURL}
            crossOrigin="anonymous"
          />
          <div className="video__controls d-flex align-items-center justify-content-center px-4">
            {this.renderPlayingButton()}
            <ProgressBar
              currentProgress={currentProgress}
              onProgressChange={this.onProgressChange}
            />
          </div>
        </div>
      </div>
    );
  }
}


export default VideoPlayer;
