import React from 'react';
import PropTypes from 'prop-types';
import StarIcon from '../../../svg/starIcon';
import './annotation.css';

const Annotation = ({
  position, onProgressChange, image, description,
}) => (
  <div className="video__progress-slider-annotation d-flex align-items-center flex-column" style={{ left: position }}>
    <div className="video__annotation-icon" onClick={onProgressChange}>
      <StarIcon />
    </div>
    <div className="video__annotation">
      <img className="video__annotation-thumb" src={image} alt="thumbnail" />
      <div className="video__annotation-description">{description}</div>
    </div>
  </div>
);

Annotation.propTypes = {
  position: PropTypes.number,
  image: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  onProgressChange: PropTypes.func.isRequired,
};

Annotation.defaultProps = {
  position: 0,
};

export default Annotation;
