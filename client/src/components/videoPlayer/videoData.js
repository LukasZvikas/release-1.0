import ImageOne from './thumbnails/image1.png';
import ImageTwo from './thumbnails/image2.png';
import ImageThree from './thumbnails/image3.png';

const videoData = [
  { timeFrame: 41, image: ImageOne, description: 'This is me hitting my serve to start the point! Just look at that...' },
  { timeFrame: 69, image: ImageTwo, description: "Here I'm hitting my forehand and trying to put pressure on my opponent!" },
  { timeFrame: 80, image: ImageThree, description: "Here I'm trying to demonstrate my backhand! Could be better..." },
];

export default videoData;
