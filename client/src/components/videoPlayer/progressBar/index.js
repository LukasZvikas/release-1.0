import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import Annotation from '../annotation';
import videoData from '../videoData';
import './progressBar.css';

const ProgressBar = ({
  currentProgress, onProgressChange,
}) => {
  const progressBar = useRef(0);
  const progressBarWidth = progressBar.current.clientWidth;


  const calculatePosition = (annotationPos) => {
    const pos = annotationPos * progressBarWidth / 100;
    return pos;
  };

  const renderAnnotations = timeFrameArr => timeFrameArr.map((item) => {
    const position = calculatePosition(item.timeFrame);
    return (
      <span key={position}>
        <Annotation
          position={position - 10}
          image={item.image}
          description={item.description}
          onProgressChange={() => onProgressChange(item.timeFrame)}
        />
      </span>
    );
  });

  return (
    <div className="video__progress-slider" ref={progressBar}>
      <input className="video__progress-slider-input" type="range" min={0} max={100} value={currentProgress} onChange={e => onProgressChange(e.target.value)} />
      {progressBarWidth ? renderAnnotations(videoData) : null}
    </div>

  );
};

ProgressBar.propTypes = {
  currentProgress: PropTypes.number.isRequired,
  onProgressChange: PropTypes.func.isRequired,
};

export default ProgressBar;
