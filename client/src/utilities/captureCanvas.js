const captureCanvas = ({ canvas, video }) => {
  canvas.width = video.clientWidth;
  canvas.height = video.clientHeight;
  canvas.getContext('2d').drawImage(video, 0, 0, video.clientWidth, video.clientHeight);
  return canvas.toDataURL('image/png');
};

export default captureCanvas;
